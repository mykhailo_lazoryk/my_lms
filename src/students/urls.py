from django.urls import path

from students.views import (
    get_all_students,
    CreateStudentView,
    UpdateStudentView,
    delete_student,
    test,
    search_history,
    Login,
    Logout,
    Registration,
    send_test_email,
    ActivateUser,
)

app_name = "students"

urlpatterns = [
    path("", get_all_students, name="all_students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:uuid>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<uuid:pk>/", delete_student, name="delete_student"),
    path("search-history/", search_history, name="search_history"),
    path("test/", test, name="test_view"),
    path("login/", Login.as_view(), name="login"),
    path("logout/", Logout.as_view(), name="logout"),
    path("registration/", Registration.as_view(), name="registration"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
    path("send-test-email/", send_test_email, name="test_send_email"),
]
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# www.facebook.com/students/ - all
# www.facebook.com/students/create/ - create
# www.facebook.com/students/15 - get one element
# www.facebook.com/students/update/15 - update
# www.facebook.com/students/delete/15 - update

import datetime

from django.conf import settings
from django.contrib.auth import get_user_model, login
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import CreateView, TemplateView, UpdateView, RedirectView
from webargs import fields
from webargs.djangoparser import use_kwargs

from .common_utils.token_generator import TokenGenerator
from .forms import StudentForm, RegistrationForm
from .models import Student
from .services.emails import send_registration_email


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ["get"]

    # def get_context_data(self):
    #     context = super().get_context_data()
    #     context["key"] = ["a", "B", "c"]
    #     return context


@use_kwargs(
    {
        "first_name": fields.Str(missing=None),
        "last_name": fields.Str(missing=None),
        "search_text": fields.Str(missing=None),
    },
    location="query",
)
def get_all_students(request, **kwargs):
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for parameter_name, parameter_value in kwargs.items():
        if parameter_value:
            if parameter_name == "search_text":
                request.session[f"search_text_{datetime.datetime.now()}"] = parameter_value
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__icontains": parameter_value})
                students = students.filter(or_filter)
            else:
                students = Student.objects.filter(**{parameter_name: parameter_value})

    return render(request, template_name="students_list.html", context={"students": students})


def search_history(request):
    log_list = {}
    for key, value in request.session.items():
        if "search_text_" in key:
            log_list.update({key: value})

    return render(request, template_name="search_history.html", context={"search_logs": log_list})


# def create_student(request):
#     if request.method == "POST":
#         form = StudentForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse("students:all_students"))
#     else:
#         form = StudentForm()
#
#     return render(request, template_name="students_create.html",
#                   context={"form": form})


class CreateStudentView(CreateView):
    model = Student
    form_class = StudentForm
    template_name = "students_create.html"
    success_url = reverse_lazy("students:all_students")


# not authorized
# login
# reset password
# registration
# registration via email

# authorized
# show username + photo
# profile
# *setting
# change password
# logout

# admin
# profile
# admin panel
# *setting
# logout
class UpdateStudentView(UpdateView):
    form_class = StudentForm
    # model = Student
    template_name = "students_update.html"
    success_url = reverse_lazy("students:all_students")
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()
    # initial = {
    #     'first_name': 'default',
    #     'last_name': 'default',
    # }


# def update_student(request, pk):
#     student = get_object_or_404(Student.objects.all(), pk=pk)
#
#     if request.method == "POST":
#         form = StudentForm(request.POST, instance=student)
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse("students:all_students"))
#     else:
#         form = StudentForm(instance=student)
#
#     response = f"""<form method="POST">
#     {form.as_p()}
#     <button type="submit" value="Submit">Submit</button>
# </form>"""
#
#     return HttpResponse(response)


def delete_student(request, pk):
    student = get_object_or_404(Student, pk=pk)
    student.delete()

    return HttpResponseRedirect(reverse("students:all_students"))


def test(request):
    # student = Student.objects.get(uuid='aab9069c-1270-46af-b28b-b41d9a793151')
    # student1 = Student.objects.filter(uuid='aab9069c-1270-46af-b28b-b41d9a793151')

    # all_group_students = Student.objects.filter(group=1)

    # print(type(group.students.all()))
    # print(student.group)
    # print(type(student.group))
    # print(list(student1))
    # student = Student()
    # student.name = 'Student1'
    # Student.objects.create(
    #     first_name=
    # )
    # group = Group.objects.get(id=1)
    # i = 4
    # teacher = Teacher()
    # teacher.first_name = f"Test-firstname-student{i}"
    # teacher.last_name = f"Test-lastname-student{i}"
    # teacher.email = f"Test-email-student{i}"
    # teacher.save()
    # teacher.group.set([group1, group2, group3])
    # teacher.save()
    # id teacher_id group_id
    # 1  1          1
    # 2  1          2
    # 3  1          3

    # _user = get_user_model().objects.get(pk=1)
    return HttpResponse("_user.profile.phone_number")


class Login(LoginView):
    pass


class Logout(LogoutView):
    pass


class Registration(CreateView):
    template_name = "registration/create_user.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request, user_instance=self.object)
        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)
            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data")


def send_test_email(request):
    send_mail(
        subject="test email",
        message="Hello from LMS",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[settings.EMAIL_HOST_USER],
    )
    return HttpResponse("Done")

from django.contrib import admin  # NOQA

from .models import Group, Teacher, Student, Profile

# Register your models here.
admin.site.register([Group, Teacher, Student, Profile])

from datetime import datetime
from uuid import uuid4

from django.contrib.auth import get_user_model

# from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils import timezone
from faker import Faker

from django.utils.translation import gettext_lazy as _

from students.managers import CustomerManager, PeopleManager  # , PeopleManager
from students.utils import first_name_validator

from django.contrib.auth.models import PermissionsMixin


# 1. Proxy model
# 2. UserProfile
# 3. AbstractUser
# 4. AbstractBaseUser
class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("name"), max_length=150, blank=True)
    last_name = models.CharField(_("surname"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), unique=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    avatar = models.ImageField(upload_to="static/img/profiles/")

    objects = CustomerManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_working_time(self):
        return f"Time on site: {timezone.now() - self.date_joined}"


class ProxyUser(get_user_model()):
    people = PeopleManager()

    class Meta:
        proxy = True
        ordering = ("-pk",)

    def do_something(self):
        print(f"{self.first_name}_{self.email}")


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    photo = models.ImageField(upload_to="static/img/profiles/")
    phone_number = models.CharField(max_length=16)
    birth_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return f"{self.user.first_name} {self.user.last_name} {self.user.email} {self.user.pk}"


class Person(models.Model):
    first_name = models.CharField(
        max_length=100,
        null=True,
        validators=[MinLengthValidator(2), first_name_validator],
    )
    last_name = models.CharField(
        max_length=100,
        null=True,
        validators=[
            MinLengthValidator(2),
        ],
    )
    email = models.EmailField(max_length=100, null=True)
    birth_date = models.DateField(null=True, default=datetime.now())
    grade = models.PositiveSmallIntegerField(default=0, null=True)

    def age(self):
        return datetime.now().year - self.birth_date.year

    class Meta:
        abstract = True


# User


class Student(Person):
    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4,
        unique=True,
        db_index=True,
    )
    group = models.ForeignKey(to="students.Group", on_delete=models.CASCADE, related_name="students")

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.pk}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for i in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-30y", end_date="-18y"),
            )


class Group(models.Model):
    name = models.CharField(max_length=100, null=True)
    count_of_students = models.PositiveSmallIntegerField(default=20, null=True)

    def __str__(self):
        return f"{self.name}:{self.pk}"


class Teacher(Person):
    first_name = models.CharField(
        max_length=100,
        null=True,
        validators=[MinLengthValidator(2)],
    )
    group = models.ManyToManyField(to="students.Group", related_name="teachers")

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.pk}"
